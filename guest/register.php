<?php include('../includes/header.php'); ?>

<nav class="navbar navbar-expand-md bg-light">
	<div class="container">
		<a class="navbar-brand" href="../index.php"><img src="../assets/images/1ecom-store-logo.png" alt=""></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbar">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="../index.php">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="shop.php">Shop</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="../customer/my_account.php">My Account</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="cart.php">Shopping Cart</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="contact.php">Contact Us</a>
				</li>

			</ul>


			<!-- SEARCH BUTTON-->
			<form class="form-group">
				<input class="form-control" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-search" type="submit">
					<i class="fa fa-search"></i>
				</button>



			</form>

			<a href="cart.php" class="btn navbar-btn btn-cart right">
				<i class="fa fa-shopping-cart"></i>
				<span>0 Items</span>
			</a>

		</div>
	</div>
</nav>

<section id="register">
	<div class="container">

		<div>
			<ul class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">Home</a></li>
				<li class="breadcrumb-item active">Register</li>
			</ul>
		</div>
		<div class="row">

			<aside class="col-md-2">
				<div class="categories">
					<?php include('../includes/sidebar.php'); ?>
				</div>
			</aside>

			<!--CONTACT US-->

			<div class="col-md-10">
				<div class="card box">
					<div class="card-header">
						<h2>Register a new Account</h2>
						<p class="text-muted">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum ducimus quibusdam perferendis alias possimus placeat ipsam maiores sunt in.
						</p>
					</div>
					<div class="card-body">
						<form action="register.php" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="name">Full Name</label>
								<input name="cName" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="email">Email</label>
								<input name="cEmail" type="email" required class="form-control">
							</div>

							<div class="form-group">
								<label for="password">Password</label>
								<input name="password" type="password" required class="form-control">
							</div>

							<div class="form-group">
								<label for="country">Country</label>
								<input name="country" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="city">City</label>
								<input name="city" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="address">Address</label>
								<input name="address" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="contact">Contact Number</label>
								<input name="contact" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="contact">Profile Image</label>
								<input name="profImage" type="file" required class="form-control">
							</div>

							<div class="text-center">
								<button type="submit" name="register" class="btn btn-details">Register</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include('../includes/footer.php'); ?>
