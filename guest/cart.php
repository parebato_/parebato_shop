<?php include('../includes/header.php'); ?>

<nav class="navbar navbar-expand-md bg-light">
	<div class="container">
		<a class="navbar-brand" href="../index.php"><img src="../assets/images/1ecom-store-logo.png" alt=""></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbar">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="../index.php">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="shop.php">Shop</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="../customer/my_account.php">My Account</a>
				</li>

				<li class="nav-item active">
					<a class="nav-link" href="cart.php">Shopping Cart</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="contact.php">Contact Us</a>
				</li>

			</ul>


			<!-- SEARCH BUTTON-->
			<form class="form-group">
				<input class="form-control" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-search" type="submit">
					<i class="fa fa-search"></i>
				</button>



			</form>

			<a href="cart.php" class="btn navbar-btn btn-cart right">
				<i class="fa fa-shopping-cart"></i>
				<span>0 Items</span>
			</a>

		</div>
	</div>
</nav>

<div class="container">
	<div>
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="index.php">Home</a></li>
			<li class="breadcrumb-item active">Cart</li>
		</ul>
	</div>

	<div class="row">
		<div id="cart" class="col-md-9">
			<div class="box">
				<div class="caption">
					<h1>Shopping Cart</h1>
					<p class="text-muted">You have 0 item(s) in your cart.</p>
				</div>
				<form action="cart.php" method="post" enctype="multipart/form-data">
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="text-center" colspan="2">Product</th>
								<th>Qty</th>
								<th>Unit Price</th>
								<th>Size</th>
								<th colspan="1">Delete</th>
								<th colspan="2">Subtotal</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td><img src="../assets/images/product2.jpg" alt=""></td>
								<td><a href="#">Locross T-shirt for Men and Women</a></td>
								<td>2</td>
								<td>$ 50</td>
								<td>Large</td>
								<td><input type="checkbox" name="remove[]"></td>
								<td>$ 100</td>
							</tr>

							<tr>
								<td><img src="../assets/images/product3.jpg" alt=""></td>
								<td><a href="#">Locross T-shirt for Men and Women</a></td>
								<td>2</td>
								<td>$ 50</td>
								<td>Large</td>
								<td><input type="checkbox" name="remove[]"></td>
								<td>$ 100</td>
							</tr>
						</tbody>

						<tfoot>
							<tr>
								<th colspan="5">Total</th>
								<th colspan="2">$200</th>
							</tr>
						</tfoot>

					</table>
					<div class="cart-btns">
						<div class="cont-shopping">
							<a href="../guest/shop.php" class="btn btn-details">
								<i class="fas fa-chevron-left"></i>Continue Shopping
							</a>
						</div>

						<div class="update-cart ml-auto">
							<a href="cart.php" class="btn btn-details">
								<i class="fas fa-sync-alt"></i>Update Cart
							</a>
						</div>

						<div class="checkout-cart">
							<a href="checkout.php" class="btn btn-details">
								<i class="fas fa-shopping-cart"></i>Proceed Checkout
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div id="summary" class="col-md-3">
			<div class="box">
				<h2>Order Summary</h2>
				<p class="text-muted">
					Shipping and additional cost are calculated based on value entered.
				</p>
				<table class="table">
					<tbody>
						<tr>
							<td>Order Subtotal</td>
							<td>$200</td>
						</tr>

						<tr>
							<td>Shipping and Handling</td>
							<td>$10</td>
						</tr>

						<tr>
							<td>Tax</td>
							<td>$0</td>
						</tr>

						<tr class="total">
							<td>Total</td>
							<td>$200</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
	<!--SUGGESTED PRODUCTS-->
	<div class="suggested">
		<h1>Suggested Products</h1>
		<div class="row">
			<div class="col-md-2 col-sm-3">
				<div class="card product">
					<a href="details.php">
						<img class="card-img-top" src="../assets/images/product1.jpg" alt="">
					</a>
					<div class="text">
						<h3><a href="details.php">Locross For Office</a></h3>
						<p class="price">$50</p>
					</div>
				</div>
			</div>

			<div class="col-md-2 col-sm-3">
				<div class="card product">
					<a href="details.php">
						<img class="card-img-top" src="../assets/images/product2.jpg" alt="">
					</a>
					<div class="text">
						<h3><a href="details.php">Locross For Office</a></h3>
						<p class="price">$50</p>
					</div>
				</div>
			</div>

			<div class="col-md-2 col-sm-3">
				<div class="card product">
					<a href="details.php">
						<img class="card-img-top" src="../assets/images/product3.jpg" alt="">
					</a>
					<div class="text">
						<h3><a href="details.php">Locross For Office</a></h3>
						<p class="price">$50</p>
					</div>
				</div>
			</div>

			<div class="col-md-2 col-sm-3">
				<div class="card product">
					<a href="details.php">
						<img class="card-img-top" src="../assets/images/product4.jpg" alt="">
					</a>
					<div class="text">
						<h3><a href="details.php">Locross For Office</a></h3>
						<p class="price">$50</p>
					</div>
				</div>
			</div>

			<div class="col-md-2 col-sm-3">
				<div class="card product">
					<a href="details.php">
						<img class="card-img-top" src="../assets/images/product5.jpg" alt="">
					</a>
					<div class="text">
						<h3><a href="details.php">Locross For Office</a></h3>
						<p class="price">$50</p>
					</div>
				</div>
			</div>

			<div class="col-md-2 col-sm-3">
				<div class="card product">
					<a href="details.php">
						<img class="card-img-top" src="../assets/images/product3.jpg" alt="">
					</a>
					<div class="text">
						<h3><a href="details.php">Locross For Office</a></h3>
						<p class="price">$50</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php include('../includes/footer.php'); ?>
