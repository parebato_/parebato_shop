<?php include('../includes/header.php'); ?>
<nav class="navbar navbar-expand-md bg-light">
    <div class="container">
        <a class="navbar-brand" href="../index.php"><img src="../assets/images/1ecom-store-logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="shop.php">Shop</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../customer/my_account.php">My Account</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="cart.php">Shopping Cart</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="contact.php">Contact Us</a>
                </li>

            </ul>



            <!-- SEARCH BUTTON-->
            <form class="form-group">
                <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-search" type="submit">
                    <i class="fa fa-search"></i>
                </button>



            </form>

            <a href="cart.php" class="btn navbar-btn btn-cart right">
                <i class="fa fa-shopping-cart"></i>
                <span>0 Items</span>
            </a>

        </div>
    </div>
</nav>

<div id="contactUs">
    <div class="container">
        <div>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item">Contact Us</li>
            </ul>
        </div>
        <div class="row">

            <aside class="col-md-2">
                <div class="categories">
                    <?php include('../includes/sidebar.php'); ?>
                </div>
            </aside>

            <!--CONTACT US-->

            <div class="col-md-10">
                <div class="card box">
                    <div class="card-header">
                        <h2>Get in touch</h2>
                        <p class="text-muted">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum ducimus quibusdam perferendis alias possimus placeat ipsam maiores sunt in, ad accusantium totam debitis esse aspernatur.
                        </p>
                    </div>
                    <div class="card-body">
                        <form action="contact.php" method="post">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input name="name" type="text" required class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input name="email" type="email" required class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="contact">Contact</label>
                                <input name="contact" type="text" required class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input name="subject" type="text" required class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea name="message" type="text" required class="form-control"></textarea>
                            </div>

                            <div class="text-center">
                                <button type="submit" name="submit" class="btn btn-details">Send Message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../includes/footer.php'); ?>
