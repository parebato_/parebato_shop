<?php include('../includes/header.php'); ?>

<nav class="navbar navbar-expand-md bg-light">
    <div class="container">
        <a class="navbar-brand" href="../index.php"><img src="../assets/images/1ecom-store-logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="shop.php">Shop</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../customer/my_account.php">My Account</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="cart.php">Shopping Cart</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="contact.php">Contact Us</a>
                </li>

            </ul>



            <!-- SEARCH BUTTON-->
            <form class="form-group">
                <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-search" type="submit">
                    <i class="fa fa-search"></i>
                </button>



            </form>

            <a href="cart.php" class="btn navbar-btn btn-cart right">
                <i class="fa fa-shopping-cart"></i>
                <span>0 Items</span>
            </a>

        </div>
    </div>
</nav>

<div class="container">

    <div>
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item">Shop</li>
            <li class="breadcrumb-item">Cart</li>
        </ul>
    </div>

    <div class="row">

        <!--SIDEBAR//CATEGORIES-->
        <aside class="col-md-2">
            <div class="categories">
                <?php include('../includes/sidebar.php'); ?>
            </div>
        </aside>


        <div class="col-md-10">
            <div class="row item-details">

                <div class="col-md-6">
                    <div id="itemCarousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#itemCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#itemCarousel" data-slide-to="1"></li>
                            <li data-target="#itemCarousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="../assets/images/product3.jpg" alt="item" class="d-block w-100">
                            </div>
                            <div class="carousel-item">
                                <img src="../assets/images/product4.jpg" alt="item" class="d-block w-100">
                            </div>
                            <div class="carousel-item">
                                <img src="../assets/images/product5.jpg" alt="item" class="d-block w-100">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#itemCarousel" role="button" data-slide="prev">
                            <span class="icon fas fa-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#itemCarousel" role="button" data-slide="next">
                            <span class="icon fas fa-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 selection">
                    <div class="box">
                        <h1>Locros Shirt for Men and Women</h1>
                        <p class="price">Price: $25.00</p>
                        <form action="details.php" method="post">
                            <div class="form-group">
                                <label for="quantity" class="col-md-5 control-label">Quantity</label>
                                <div class="col-md-5">
                                    <select name="quantity" class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="size" class="col-md-5 control-label">Size</label>
                                <div class="col-md-5">
                                    <select name="size" class="form-control">
                                        <option>XS</option>
                                        <option>SM</option>
                                        <option>L</option>
                                        <option>M</option>
                                    </select>
                                </div>
                            </div>

                            <a class="btn btn-cart" href="cart.php">
                                <i class="fa fa-shopping-cart">
                                    <span>Add To Cart</span>
                                </i>
                            </a>

                        </form>
                    </div>

                    <!--PRODUCT IMAGE THUMBNAILS-->

                    <div class="row" id="thumbs">
                        <div class="col-sm-4">
                            <a data-target="#itemCarousel" data-slide-to="0" href="" class="thumb box">
                                <img src="../assets/images/product3.jpg" alt="image 1" class="img-fluid">
                            </a>
                        </div>

                        <div class="col-sm-4">
                            <a data-target="#itemCarousel" data-slide-to="1" href="" class="thumb box">
                                <img src="../assets/images/product4.jpg" alt="image 2" class="img-fluid">
                            </a>
                        </div>

                        <div class="col-sm-4">
                            <a data-target="#itemCarousel" data-slide-to="2" href="" class="thumb box">
                                <img src="../assets/images/product5.jpg" alt="image 2" class="img-fluid">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!--PRODUCT DETAILS-->

            <div class="details">
                <div class="box">
                    <h4>Product Details</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed repellendus explicabo a, adipisci voluptatibus culpa porro tenetur, asperiores mollitia eius veritatis. Doloremque quis cum amet perferendis at quo veritatis, dignissimos.
                    </p>
                    <h4>Sizes</h4>
                    <ul>
                        <li>Small</li>
                        <li>Medium</li>
                        <li>Large</li>
                    </ul>
                    <hr>
                </div>
            </div>

            <!--PRODUCTS YOU MAY LIKE-->
            <div class="suggested">
                <h1>Suggested Products</h1>
                <div class="row">
                    <div class="col-md-2 col-sm-3">
                        <div class="card product">
                            <a href="details.php">
                                <img class="card-img-top" src="../assets/images/product1.jpg" alt="">
                            </a>
                            <div class="text">
                                <h3><a href="details.php">Locross For Office</a></h3>
                                <p class="price">$50</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-3">
                        <div class="card product">
                            <a href="details.php">
                                <img class="card-img-top" src="../assets/images/product2.jpg" alt="">
                            </a>
                            <div class="text">
                                <h3><a href="details.php">Locross For Office</a></h3>
                                <p class="price">$50</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-3">
                        <div class="card product">
                            <a href="details.php">
                                <img class="card-img-top" src="../assets/images/product3.jpg" alt="">
                            </a>
                            <div class="text">
                                <h3><a href="details.php">Locross For Office</a></h3>
                                <p class="price">$50</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-3">
                        <div class="card product">
                            <a href="details.php">
                                <img class="card-img-top" src="../assets/images/product4.jpg" alt="">
                            </a>
                            <div class="text">
                                <h3><a href="details.php">Locross For Office</a></h3>
                                <p class="price">$50</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-3">
                        <div class="card product">
                            <a href="details.php">
                                <img class="card-img-top" src="../assets/images/product5.jpg" alt="">
                            </a>
                            <div class="text">
                                <h3><a href="details.php">Locross For Office</a></h3>
                                <p class="price">$50</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-3">
                        <div class="card product">
                            <a href="details.php">
                                <img class="card-img-top" src="../assets/images/product3.jpg" alt="">
                            </a>
                            <div class="text">
                                <h3><a href="details.php">Locross For Office</a></h3>
                                <p class="price">$50</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>



<?php include('../includes/footer.php'); ?>
