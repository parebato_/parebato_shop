<?php include('../includes/header.php'); ?>

<nav class="navbar navbar-expand-md bg-light">
    <div class="container">
        <a class="navbar-brand" href="../index.php"><img src="../assets/images/1ecom-store-logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="shop.php">Shop</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../customer/my_account.php">My Account</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="cart.php">Shopping Cart</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="contact.php">Contact Us</a>
                </li>

            </ul>



            <!-- SEARCH BUTTON-->
            <form class="form-group">
                <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-search" type="submit">
                    <i class="fa fa-search"></i>
                </button>



            </form>

            <a href="cart.php" class="btn navbar-btn btn-cart right">
                <i class="fa fa-shopping-cart"></i>
                <span>0 Items</span>
            </a>

        </div>
    </div>
</nav>

<section id="shop">
    <div class="container">

        <div>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active">Shop</li>
                <li class="breadcrumb-item">Cart</li>
            </ul>
        </div>

        <div class="row">

            <!--SIDEBAR//CATEGORIES-->
            <aside class="col-md-2">
                <div class="categories">
                    <?php include('../includes/sidebar.php'); ?>
                </div>
            </aside>


            <div class="col-md-10">

                <div class="shop">
                    <div class="card box">
                        <h1 class="header">Shop</h1>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus est odit, at ullam, sunt harum consequatur maxime, autem iusto repellat cupiditate impedit eligendi distinctio alias. Beatae, ea rerum quae excepturi.
                        </p>
                    </div>

                    <div class="row products">
                        <div class="col-sm-4 col-md-3">
                            <div class="card">
                                <a href="details.php">
                                    <img class="img-fluid" src="../assets/images/product1.jpg" alt="product">
                                </a>
                                <div class="text">
                                    <h3><a href="details.php">Locross T-Shirt</a></h3>
                                    <p class="price">$30</p>
                                    <div class="button">
                                        <a class="btn btn-details" href="details.php">View Details</a>
                                        <a class="btn btn-cart" href="details.php">
                                            <i class="fa fa-shopping-cart">
                                                <span>Add To Cart</span>
                                            </i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-md-3">
                            <div class="card">
                                <a href="details.php">
                                    <img class="img-fluid" src="../assets/images/product2.jpg" alt="product">
                                </a>
                                <div class="text">
                                    <h3><a href="details.php">Locross T-Shirt</a></h3>
                                    <p class="price">$30</p>
                                    <div class="button">
                                        <a class="btn btn-details" href="details.php">View Details</a>
                                        <a class="btn btn-cart" href="details.php">
                                            <i class="fa fa-shopping-cart">
                                                <span>Add To Cart</span>
                                            </i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-md-3">
                            <div class="card">
                                <a href="details.php">
                                    <img class="img-fluid" src="../assets/images/product3.jpg" alt="product">
                                </a>
                                <div class="text">
                                    <h3><a href="details.php">Locross T-Shirt</a></h3>
                                    <p class="price">$30</p>
                                    <div class="button">
                                        <a class="btn btn-details" href="details.php">View Details</a>
                                        <a class="btn btn-cart" href="details.php">
                                            <i class="fa fa-shopping-cart">
                                                <span>Add To Cart</span>
                                            </i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-md-3">
                            <div class="card">
                                <a href="details.php">
                                    <img class="img-fluid" src="../assets/images/product4.jpg" alt="product">
                                    <div class="text">
                                        <h3><a href="details.php">Locross T-Shirt</a></h3>
                                        <p class="price">$30</p>
                                        <div class="button">
                                            <a class="btn btn-details" href="details.php">View Details</a>
                                            <a class="btn btn-cart" href="details.php">
                                                <i class="fa fa-shopping-cart">
                                                    <span>Add To Cart</span>
                                                </i>
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!--PAGINATION-->
                    <div>
                        <ul class="pagination justify-content-center">
                            <li class="page-item"><a class="page-link" href="#">Back</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>



<!-- Start of FOOTER   -->
<?php include('../includes/footer.php'); ?>
