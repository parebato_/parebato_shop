<?php 
include('../connection/db.php'); 
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">
    <title>Insert Products</title>
    <!--  Google Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700,800,900&display=swap" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/611f1e7725.js"></script>
    <script src="assets/js/tinymce/tinymce.min.js"></script>


    <!--Reset css here-->
    <link href="assets/styles/reset.css" rel="stylesheet" type="text/css">
    <!--Custom CSS-->
    <link href="../assets/styles/insert_prod.css" rel="stylesheet" type="text/css">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ol class=" box breadcrumb">
                    <li class="active">
                        Dashboard / Insert Product
                    </li>
                </ol>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h2>Insert Product</h2>
                    </div>
                    <div class="card-body">

                        <form action="db_insert_product.php" method="POST" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="title">Product Title</label>
                                <input type="text" name="title" class="form-control">
                            </div>

                            <!--	PRODUCT CATEGORIES -->
                            <div class="form-group">
                                <label>Product Category</label>
                                <select name="product_category_id" class="form-control">
                                    <option value="">Select Product Category</option>

                                    <?php 
										
                  										$get_prodCategories = 'SELECT * FROM product_categories';
                  										$product_categories = mysqli_query($con, $get_prodCategories);
									
									                   foreach($product_categories as $product_category){
										                ?>
                                    <option value="<?php echo $product_category['id']?>"><?php echo $product_category['name']?></option>
                                    <?php
              										}
              									?>

                                </select>
                            </div>



                            <!--	GENDER CATEGORIES -->
                            <div class="form-group">
                                <label>Category</label>
                                <select name="category_id" class="form-control">
                                    <option value="">Select Category</option>

                                    <?php 
										
										$get_categories = 'SELECT * FROM categories';
										$categories = mysqli_query($con, $get_categories);
									
										foreach ($categories as $category){
											?>
                                    <option value="<?php echo $category['id']?>"><?php echo $category['name']?></option>
                                    <?php
									}
									?>

                                </select>
                            </div>

                            <!--	PRODUCT PRICE-->
                            <div class="form-group">
                                <label>Product Price</label>
                                <input type="text" name="price" class="form-control">
                            </div>
                            <!--	PRODUCT KEYWORDS-->
                            <div class="form-group">
                                <label>Product Keywords</label>
                                <input type="text" name="keywords" class="form-control">
                            </div>
                            <!--	PRODUCT DESCRIPTION-->
                            <div class="form-group">
                                <label>Product Description</label>
                                <textarea name="description" class="form-control"></textarea>
                            </div>


                            <!--	PRODUCT IMAGES-->
                            <div class="form-group">
                                <label>Upload Image</label>
                                <input name="image" type="file" multiple class="form-control">
                            </div>
                            <div class="form-group">
                                <button name="submit" type="submit" class="btn btn-details">Insert Product</button>
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="assets/js/tinymce/tinymce.min.js"></script>
    <!-- TEXT AREA script	-->
    <script>
        tinymce.init({
            selector: 'textarea'
        });

    </script>
</body>

</html>
