<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <h4>Pages</h4>
                <ul>
                    <li><a href="cart.php">Shopping Cart</a></li>
                    <li><a href="shop.php">Shop Now</a></li>
                    <li><a href="contact.php">Contact Us</a></li>
                    <li><a href="checkout.php">My Account</a></li>
                </ul>
                <hr>
                <h4>User Section</h4>
                <ul>
                    <li><a href="checkout.php">Login</a></li>
                    <li><a href="register.php">Register</a></li>
                </ul>
                <hr class="hidden-md hidden-lg hidden-sm">
            </div>
            <div class="col-sm-l col-md-3">
                <h4>Top Products Categories</h4>
                <ul>
                    <li><a href="#">Jackets</a></li>
                    <li><a href="#">Accesories</a></li>
                    <li><a href="#">Coats</a></li>
                    <li><a href="#">Shoes</a></li>
                    <li><a href="#">T-Shirts</a></li>
                </ul>
                <hr class="hidden-md hidden-lg">
            </div>
            <div class="col-sm-6 col-md-3">
                <h4>Find Us:</h4>
                <p>
                    <strong>PAREBATO Online Shop</strong>
                    <br>Mandaluyong City
                    <br>+63 917 248 2433
                    <br>patriciamarierebato@gmail.com
                </p>
                <a href="contact.php">Check our Contact Page</a>
                <hr class="hidden-md hidden-lg">
            </div>
            <div class="col-sm-6 col-md-3">
                <h4>Get News</h4>
                <p class="text-muted">
                    Don't miss our products latest updates and promotions. Sign up for newsletter now.
                </p>
                <form action="" method="post">
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <input type="submit" value="subscribe" class="btn btn-default">
                        </span>
                    </div>
                </form>
                <hr>
                <h4>Keep in Touch</h4>
                <p class="social">
                    <a href="" class="fab fa-facebook-f"></a>
                    <a href="" class="fab fa-twitter"></a>
                    <a href="" class="fab fa-instagram"></a>
                    <a href="" class="fab fa-google-plus-g"></a>
                    <a href="" class="fa fa-envelope"></a>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- COPYRIGHT   -->
<div id="copyright">
    <div class="container">
        <div class="col-md-6">
            <p class="pull-left">
                &copy; 2019 PAREBATO Store All Rights Reserve
            </p>
        </div>
        <div class="col-md-6">
            <p class="pull-right">
                Design and Developed by: PAREBATO
            </p>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>

</html>
