<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../../../favicon.ico">
	<title>Online Store</title>

	<!--  Google Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700,800,900&display=swap" rel="stylesheet">
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<script src="https://kit.fontawesome.com/611f1e7725.js"></script>

	<!--Stylesheets here-->
	<link href="../assets/styles/reset.css" rel="stylesheet" type="text/css">
	<!--Custom CSS-->
	<link href="../assets/styles/style.css" rel="stylesheet" type="text/css">
	<link href="../customer/assets/styles/style.css" rel="stylesheet" type="text/css">
	<link href="../customer/assets/styles/insert_prod.css" rel="stylesheet" type="text/css">
</head>

<body>

	<!-- Top Nav  -->

	<div id="top">
		<div class="container">
			<div class="row">
				<div class="col-md-6 offer">
					<a href="#" class="btn btn-success btn-sm">Welcome</a>
					<a href="checkout.php">4 Items In Your Cart | Total Price: $300 </a>
				</div>
				<div class="col-md-6">
					<ul class="menu">
						<li>
							<a href="register.php">Register</a>
						</li>
						<li>
							<a href="checkout.php">My Account</a>
						</li>
						<li>
							<a href="cart.php">Go To Cart</a>
						</li>
						<li>
							<a href="checkout.php">Login</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!--    MAIN NAV  -->
