	<div class="card">
		<div class="card-header">
			<h2>My Orders</h2>
			<p>Your Order all in one place</p>
			<p class="text-muted">
				Illum ducimus quibusdam perferendis alias possimus placeat ipsam maiores sunt in, ad accusantium totam debitis esse aspernatur.
			</p>
		</div>


		<!--ORDERS TABLE-->
		<div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ON:</th>
						<th>Due Amount:</th>
						<th>Qty:</th>
						<th>Size: </th>
						<th>Order Date:</th>
						<th>Paid / Unpaid</th>
						<th>Status: </th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<th>#1</th>
						<td>$80</td>
						<td>608773</td>
						<td>Small</td>
						<td>12-20-2019</td>
						<td>Unpaid</td>
						<td>
							<a href="confirm_payment.php" target="_blank" class="btn btn-details">Confirm Payment</a>
						</td>
					</tr>

					<tr>
						<th>#2</th>
						<td>$80</td>
						<td>608773</td>
						<td>Small</td>
						<td>12-20-2019</td>
						<td>Unpaid</td>
						<td>
							<a href="confirm_payment.php" target="_blank" class="btn btn-details">Confirm Payment</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
