				<div class="card edit_prof">
					<div class="card-header">
						<h2>Edit Your Profile Information</h2>
						<p class="text-muted">
							Illum ducimus quibusdam perferendis alias possimus placeat ipsam maiores sunt in.
						</p>
					</div>
					<div class="card-body">
						<form action="edit_account.php" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="name">Full Name</label>
								<input name="cName" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="email">Email</label>
								<input name="cEmail" type="email" required class="form-control">
							</div>

							<div class="form-group">
								<label for="password">Password</label>
								<input name="password" type="password" required class="form-control">
							</div>

							<div class="form-group">
								<label for="country">Country</label>
								<input name="country" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="city">City</label>
								<input name="city" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="address">Address</label>
								<input name="address" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="contact">Contact Number</label>
								<input name="contact" type="text" required class="form-control">
							</div>

							<div class="form-group">
								<label for="contact">Profile Image</label>
								<input name="profImage" type="file" required class="form-control">
							</div>
							<div class="profImage">
								<img src="../customer/assets/images/profile.jpg" alt="">
							</div>

							<div class="text-center">
								<button type="submit" name="register" class="btn btn-details">Save Changes
								</button>
							</div>

						</form>
					</div>

				</div>
