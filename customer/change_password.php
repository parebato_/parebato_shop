<div class="card new_pass">
	<div class="card-header">
		<h2>Change Password</h2>
		<p class="text-muted">
			Illum ducimus quibusdam perferendis alias possimus placeat ipsam maiores sunt in.
		</p>
	</div>
	<div class="card-body">
		<form action="change_password.php" method="post" enctype="multipart/form-data">

			<div class="form-group">
				<label for="oldPass">Old Password</label>
				<input name="oldPass" type="password" required class="form-control">
			</div>

			<div class="form-group">
				<label for="newPass">New Password</label>
				<input name="newPass" type="password" required class="form-control">
			</div>

			<div class="form-group">
				<label for="confirmPass">Confirm new Password</label>
				<input name="confirmPass" type="text" required class="form-control">
			</div>

			<div class="text-center">
				<button type="submit" name="register" class="btn btn-details">Save New Password
				</button>
			</div>
		</form>
	</div>
</div>
