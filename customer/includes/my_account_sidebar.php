<div class="card">
	<img src="assets/images/profile.jpg" alt="profile image" class="card-img-top">
	<div class="card-body">
		<h2 class="name">Patricia Camongol</h2>
		<ul class="list-group list-group-flush">

			<li class="list-group-item
											<?php if(isset($_GET['my_orders']))
														{echo 'active';} ?>">

				<a href="my_account.php?my_orders"><i class="fas fa-list-ul"></i>My Orders</a> </li>


			<li class="list-group-item
											<?php if(isset($_GET['pay_offline']))
														{echo 'active';} ?>">
				<a href="my_account.php?pay_offline"><i class="fas fa-bolt"></i>Pay Offline</a></li>



			<li class="list-group-item
											<?php if(isset($_GET['edit_account']))
														{echo 'active';} ?>">

				<a href="my_account.php?edit_account"><i class="fas fa-pencil-alt"></i>Edit Account</a></li>



			<li class="list-group-item
											<?php if(isset($_GET['change_password']))
														{echo 'active';} ?>">

				<a href="my_account.php?change_password"><i class="fas fa-unlock-alt"></i>Change Password</a></li>


			<li class="list-group-item
											<?php if(isset($_GET['delete_account']))
														{echo 'active';} ?>">

				<a href="my_account.php?delete_account" data-toggle="modal" data-target="#deleteAccount"><i class="fas fa-trash-alt"></i>Delete Account</a></li>



			<li class="list-group-item
											<?php if(isset($_GET['logout']))
														{echo 'active';} ?>">

				<a href="my_account.php?logout"><i class="fas fa-user-shield"></i>Log Out</a></li>


		</ul>
	</div>
</div>
