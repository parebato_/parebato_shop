<?php include('../includes/header.php'); ?>

<nav class="navbar navbar-expand-md bg-light">
	<div class="container">
		<a class="navbar-brand" href="../index.php"><img src="../assets/images/1ecom-store-logo.png" alt=""></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbar">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="../index.php">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../guest/shop.php">Shop</a>
				</li>

				<li class="nav-item active">
					<a class="nav-link" href="my_account.php">My Account</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="../guest/cart.php">Shopping Cart</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="../guest/contact.php">Contact Us</a>
				</li>

			</ul>



			<!-- SEARCH BUTTON-->
			<form class="form-group">
				<input class="form-control" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-search" type="submit">
					<i class="fa fa-search"></i>
				</button>



			</form>

			<a href="cart.php" class="btn navbar-btn btn-cart right">
				<i class="fa fa-shopping-cart"></i>
				<span>0 Items</span>
			</a>

		</div>
	</div>
</nav>

<div id="myAccount">
	<div class="container">
		<div>
			<ul class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">Home</a></li>
				<li class="breadcrumb-item active">My Account</li>
			</ul>
		</div>


		<div class="row">

			<div class="col-md-3 profile-box">
				<!--INCLUDE CARD IN MY ACCOUNT SIDEBAR FROME HERE...-->
				<?php include ('includes/my_account_sidebar.php'); ?>
			</div>


			<!--		CONFIRM PAYMENT FORM 	-->
			<div class="col-md-9">
				<div class="payment card">
					<h2 class="card-header">Confirm your payment</h2>
					<div>
						<form action="confirm_payment.php" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="">Invoice No:</label>
								<input type="text" name="invoice_no" class="form-control" required>
							</div>

							<div class="form-group">
								<label for="">Amount Sent:</label>
								<input type="text" name="amount_sent" class="form-control" required>
							</div>

							<div class="form-group">
								<label for="">Payment Type:</label>
								<select name="payment_mode" class="form-control" required>
									<option>Select Payment Mode</option>
									<option>Bank Code</option>
									<option>Paypal</option>
								</select>
							</div>

							<div class="form-group">
								<label for="">Transaction / Reference No: </label>
								<input type="text" name="ref_no" class="form-control" required>
							</div>

							<div class="form-group">
								<label for=""> Omni Paisa / East Paisa: </label>
								<input type="text" name="code" class="form-control" required>
							</div>

							<div class="form-group">
								<label for=""> Payment Date: </label>
								<input type="text" name="date" class="form-control" required>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-details">Confirm Payment</button>
							</div>

						</form>
					</div>

				</div>
			</div>
		</div>



		<?php include('../includes/footer.php'); ?>
