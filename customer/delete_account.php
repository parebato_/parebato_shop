<div class="modal fade" id="deleteAccount" tabindex="-1" role="dialog" aria-labelledby="deleteTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h2 class="modal-title" id="deleteTitle">Are you sure you want to delete your account?</h2>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				All information stored about your account will be deleted and cannot be retrieved anymore.
			</div>
			<div class="modal-footer">
				<form action="" method="post">
					<input type="submit" name="yes" value="Delete" class="btn btn-danger">
				</form>
			</div>
		</div>
	</div>
</div>