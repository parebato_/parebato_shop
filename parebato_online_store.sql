-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2019 at 01:41 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parebato_online_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Men', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.'),
(2, 'Women', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.'),
(3, 'Kids', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.'),
(4, 'Others', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.');

-- --------------------------------------------------------

--
-- Table structure for table `index_slider`
--

CREATE TABLE `index_slider` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `index_slider`
--

INSERT INTO `index_slider` (`id`, `name`, `image`) VALUES
(1, 'index slider 1', 'bg1.jpg'),
(2, 'index slider 2', 'bg2.jpg'),
(3, 'index slider 3', 'bg3.jpg'),
(4, 'index slider 4', 'bg4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `product_category_id` int(11) UNSIGNED NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL,
  `price` varchar(255) NOT NULL,
  `keywords` text NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `product_category_id`, `category_id`, `price`, `keywords`, `description`, `image`, `date`) VALUES
(71, 'Lady Shirt', 5, 2, '150', 'Shirt', 'Lady Shirt Red', '../admin/product_images/shirt1.jpg', '2019-12-24 12:14:39'),
(72, 'Men Shoes', 3, 1, '150', 'Running Shoes', 'Men and Lady Running Shoes', '../admin/product_images/shoes1.jpg', '2019-12-24 12:14:39'),
(73, 'DJ Coat', 4, 1, '200.00', 'Winter Coat', 'this is a good quality winter coat', '../admin/product_images/jacket2.jpg', '2019-12-24 12:14:39'),
(74, 'Winter Coat', 4, 2, '200', 'Coats', 'This is a good quality coat ', '../admin/product_images/jacket3.jpg', '2019-12-24 12:14:39'),
(75, 'Shades', 2, 3, '150', 'Shades', 'This is a Shades for a kiddos', '../admin/product_images/shades1.jpg', '2019-12-24 12:14:39'),
(76, 'Shades', 2, 1, '45', 'Shades', 'This is the number shades in China', '../admin/product_images/shades1.jpg', '2019-12-24 12:14:39'),
(77, 'She Shirt', 4, 3, '450', 'Shades She Sheert', 'This is the number shades in China She Sheert', '../admin/product_images/shirt2.jpg', '2019-12-24 12:14:39'),
(78, 'Insert Item Image check', 4, 4, '150', 'Shades', 'Insert Image Check', '../admin/product_images/shades1.jpg', '2019-12-24 12:13:06'),
(79, '', 0, 0, '', '', '', '../admin/product_images/shirt1.jpg', '2019-12-24 12:15:05'),
(81, 'Lacross T-Shirt', 1, 1, '', '', '', 'admin/product_images/shirt3.jpg', '2019-12-24 12:16:08'),
(82, 'Pat', 1, 1, '34.00', 'Jacket', 'Patricia Marie Rebato', 'admin/product_images/bedroom.jpg', '2019-12-24 12:18:02'),
(83, 'Bedroom', 1, 1, '34.00', 'Jacket', 'Bedroom', '../admin/product_images/bedroom.jpg', '2019-12-24 12:19:08'),
(84, 'Before I sleep', 3, 3, '250', 'Bid bid Bid', 'you are my song', '../admin/product_images/buildings-690249_1920.jpg', '2019-12-24 12:21:43'),
(85, 'Firefox', 1, 2, '90.00', 'FireFox', 'Firefox Firefox', '../admin/product_images/wax.jpg', '2019-12-24 12:32:33'),
(86, 'Without dot dot', 5, 3, '90.00', 'Without dot dot', 'Check if image will appear', 'admin/product_images/iron.jpg', '2019-12-24 12:34:49'),
(87, 'Irion', 2, 2, '10', 'Irion', 'iron din', 'admin/product_images/iron.jpg', '2019-12-24 12:36:45');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `description`) VALUES
(1, 'Jackets', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.'),
(2, 'Accessories', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.'),
(3, 'Shoes', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.'),
(4, 'Coats', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.'),
(5, 'T-Shirts', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `index_slider`
--
ALTER TABLE `index_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `index_slider`
--
ALTER TABLE `index_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
