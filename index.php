<?php require('connection/db.php'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">
    <title>Online Store</title>

    <!--  Google Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700,800,900&display=swap" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/611f1e7725.js"></script>

    <!--Stylesheets here-->
    <link href="assets/styles/reset.css" rel="stylesheet" type="text/css">
    <!--Custom CSS-->
    <link href="assets/styles/style.css" rel="stylesheet" type="text/css">
</head>

<body>

    <!-- Top Nav  -->

    <div id="top">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offer">
                    <a href="#" class="btn btn-success btn-sm">Welcome</a>
                    <a href="checkout.php">4 Items In Your Cart | Total Price: $300 </a>
                </div>
                <div class="col-md-6">
                    <ul class="menu">
                        <li>
                            <a href="guest/register.php">Register</a>
                        </li>
                        <li>
                            <a href="checkout.php">My Account</a>
                        </li>
                        <li>
                            <a href="cart.php">Go To Cart</a>
                        </li>
                        <li>
                            <a href="checkout.php">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--    MAIN NAV  -->

    <nav class="navbar navbar-expand-md bg-light">
        <div class="container">
            <a class="navbar-brand" href="index.php"><img src="assets/images/1ecom-store-logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbar">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="guest/shop.php">Shop</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="customer/my_account.php">My Account</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="guest/cart.php">Shopping Cart</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="guest/contact.php">Contact Us</a>
                    </li>

                </ul>


                <!-- SEARCH BUTTON-->
                <form class="form-group">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-search" type="submit">
                        <i class="fa fa-search"></i>
                    </button>



                </form>

                <a href="cart.php" class="btn navbar-btn btn-cart right">
                    <i class="fa fa-shopping-cart"></i>
                    <span>0 Items</span>
                </a>

            </div>
        </div>
    </nav>



    <!--SLIDER-->
    <div class="main-carousel">
        <div id="carouselCaptions" class="carousel slide" data-ride="carousel">
            <div class="container">
                <ol class="carousel-indicators">
                    <li data-target="#carouselCaptions" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselCaptions" data-slide-to="1"></li>
                    <li data-target="#carouselCaptions" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">


                    <?php
					
					$get_slides = 'SELECT * FROM index_slider LIMIT 0,1';
					
					$run_slides = mysqli_query($con,$get_slides);
   					
					while($row_slides = mysqli_fetch_array($run_slides)){
				$name = $row_slides['name'];
					
						$image = $row_slides['image'];
						
						echo "
                    <div class='carousel-item active'>
                        <img src='assets/images/slides/$image'>
                    </div>
						";
							};
					
					
					 //second slide and up
					 $get_slides = 'SELECT * FROM index_slider LIMIT 1,6';

					 $run_slides = mysqli_query($con,$get_slides);

					 while($row_slides = mysqli_fetch_array($run_slides)){
					 $name = $row_slides['name'];

					 $image = $row_slides['image'];

					 echo "
					 <div class='carousel-item'>
					 	<img src='assets/images/slides/$image'>
					 </div>
					 ";
						 
					 };
					
					?>


                </div>
                <a class="carousel-control-prev controllers" href="#carouselCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next controllers" href="#carouselCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <!-- Introduction    -->
    <div id="advantages">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="box">
                        <div class="icon">
                            <i class="fa fa-dollar"> </i>
                        </div>
                        <div class="caption">
                            <h3><a href="#"> Best Prices</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <div class="icon">
                            <i class="fa fa-heart"> </i>
                        </div>
                        <div class="caption">
                            <h3><a href="#"> Amazing Reviews </a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.</p>
                        </div>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <div class="icon">
                            <i class="fa fa-tag"> </i>
                        </div>
                        <div class="caption">
                            <h3><a href="#">Quality Products </a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat natus, nulla voluptatum! Placeat aut reiciendis cumque ducimus, officiis id sint rerum ipsum ratione in. Velit mollitia, laudantium fuga eum alias.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--    StoreFront-->
    <div class="container" id="latest">
        <h2>Latest Products</h2>
    </div>



    <!--PRODUCTS-->

    <div id="products" class="container">
        <div class="row">
            <!--Query to display data from database-->
            <?php
					$products_query = "SELECT * FROM products ORDER BY 1 DESC LIMIT 0,4";
					$products = mysqli_query($con, $products_query);
					
					foreach ($products as $product){
						?>
            <div class="col-sm-4 col-md-3">
                <div class="card">
                    <a href="guest/details.php?id=<?php echo $product['id']; ?>">
                        <img class="img-fluid" src="admin/<?php echo $product['image']; ?>" alt="image">
                    </a>
                    <div class="text">
                        <h3> <a href="guest/details.php?id=<?php echo $product['id']; ?>"><?php echo $product['title']; ?></a></h3>
                        <p class=" price">$<?php echo $product['price']; ?></p>
                        <div class="button">
                            <a class="btn btn-details" href="guest/details.php?id=<?php echo $product['id']; ?>">View Details</a>
                            <a class="btn btn-cart" href="guest/details.php?id=<?php echo $product['id']; ?>">
                                <i class="fa fa-shopping-cart">
                                    <span>Add To Cart</span>
                                </i>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            <?php
			}
		?>
        </div>
    </div>




    <!-- Start of FOOTER   -->
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <h4>Pages</h4>
                    <ul>
                        <li><a href="cart.php">Shopping Cart</a></li>
                        <li><a href="shop.php">Shop Now</a></li>
                        <li><a href="contact.php">Contact Us</a></li>
                        <li><a href="checkout.php">My Account</a></li>
                    </ul>
                    <hr>
                    <h4>User Section</h4>
                    <ul>
                        <li><a href="checkout.php">Login</a></li>
                        <li><a href="customer_register.php">Register</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg hidden-sm">
                </div>
                <div class="col-sm-l col-md-3">
                    <h4>Top Products Categories</h4>
                    <ul>
                        <li><a href="#">Jackets</a></li>
                        <li><a href="#">Accesories</a></li>
                        <li><a href="#">Coats</a></li>
                        <li><a href="#">Shoes</a></li>
                        <li><a href="#">T-Shirts</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4>Find Us:</h4>
                    <p>
                        <strong>PAREBATO Online Shop</strong>
                        <br>Mandaluyong City
                        <br>+63 917 248 2433
                        <br>patriciamarierebato@gmail.com
                    </p>
                    <a href="contact.php">Check our Contact Page</a>
                    <hr class="hidden-md hidden-lg">
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4>Get News</h4>
                    <p class="text-muted">
                        Don't miss our products latest updates and promotions. Sign up for newsletter now.
                    </p>
                    <form action="" method="post">
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <input type="submit" value="subscribe" class="btn btn-details">
                            </span>
                        </div>
                    </form>
                    <hr>
                    <h4>Keep in Touch</h4>
                    <p class="social">
                        <a href="" class="fab fa-facebook-f"></a>
                        <a href="" class="fab fa-twitter"></a>
                        <a href="" class="fab fa-instagram"></a>
                        <a href="" class="fab fa-google-plus-g"></a>
                        <a href="" class="fa fa-envelope"></a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- COPYRIGHT   -->
    <div id="copyright">
        <div class="container">
            <div class="col-md-6">
                <p class="pull-left">
                    &copy; 2019 PAREBATO Store All Rights Reserve
                </p>
            </div>
            <div class="col-md-6">
                <p class="pull-right">
                    Design and Developed by: PAREBATO
                </p>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>

</html>
